# Prueba practica
# inventario_productos

# Estructura del proyecto siguiendo el paradigma MVC
# carpetas
1. controllers
2. models
3. view
    - js
4. sql


Prueba practica en la cuál se desarrolla un CRUD para el inventario de productos

# Pasos para la configuración del proyecto
1. Crear la base datos con el nombre de inventario_productos 
2. Una vez creada la base datos importamos el archivo `inventario_productos.sql` que se encuentra en la carpeta sql en la raiz del proyecto
3. procedemos a clonar el proyecto en un servidor local en mi caso utilice `xampp` 
4. Situados en el administrador de archivos dentro del servidor local por medio de la terminal colocamos `git clone https://gitlab.com/ralh17/inventario_productos/`

# Funcionalidades del CRUD de producto
1. Listar productos
2. Crear producto
3. Editar producto
4. Buscar producto
5. Eliminar producto
6. Vender un producto

# Extras utilizados en el proyecto
1. Framework bootstrap para la parte de diseño de la vista, instalados por medio de CDN
2. libreria de iconos font-awesome, instalados por medio de CDN
2. Ajax con jQuery para el momento de buscar, eliminar y vender un producto
3. libreria tippy js para mostrar un mensaje cuando se le haga hover a cualquier botón en la parte de acciones

# Parametros de conexion de la base datos
`host=localhost`
`dbname=inventario_productos`
`usuario = root`
`password =`

