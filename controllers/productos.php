<?php
include_once("../models/Productos.php");
$productos = Producto::MdlListarProductos();
if (isset($_REQUEST['opcion'])) {
    include_once("../models/Productos.php");
    switch ($_REQUEST['opcion']) {
        case 'ctrCrearProducto':
            if (isset($_POST["nombreProducto"]) && !empty($_POST["nombreProducto"])) {
                $resultado = Producto::MdlCrearProducto($_POST);
                if ($resultado === "ok") {
                    echo '<script>
                            alert("El producto ha sido creado correctamente");
                            window.location = "index.php";
                      </script>';
                } else {
                    echo '<script>
                            alert("Error al crear el producto");
                            window.location = "index.php";
                      </script>';
                }
            }

            break;
        case 'ctrBuscarProducto':
            $producto = Producto::MdlBuscarProducto($_POST['productoId']);
            echo json_encode($producto);
            break;
        case 'ctrEditarProductos':
            if (isset($_POST["nombreProductoEditar"]) && !empty($_POST["nombreProductoEditar"])) {
                $resultado = Producto::MdlEditarProducto($_POST);
                if ($resultado === "ok") {
                    echo '<script>
                            alert("El producto ha sido Editado correctamente");
                            window.location = "index.php";
                      </script>';
                } else {
                    echo '<script>
                            alert("Error al editar el producto");
                            window.location = "index.php";
                      </script>';
                }
            }
            break;
        case 'ctrEliminarProducto':
            $producto = Producto::MdlEliminarProducto($_POST['productoId']);
            echo json_encode($producto);
            break;
        case 'ctrVenderProducto':
            $producto = Producto::mdlVenderProducto($_POST['productoId']);
            echo json_encode($producto);
            break;
    }
}