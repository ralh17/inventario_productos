$(document).ready(function() {
 let btnEditar = document.querySelectorAll('#btnEditarProducto');
 let btnEliminar = document.querySelectorAll('#btnEliminar');
 let btnVender = document.querySelectorAll('#btnVender');

 // tooltip al hacer hover en el boton editar
 tippy(btnEditar, {
  content: 'Editar Producto',
 });

 // tooltip al hacer hover en el boton eliminar
 tippy(btnEliminar, {
  content: 'Eliminar Producto',
 });

 // tooltip al hacer hover en el boton vender
 tippy(btnVender, {
  content: 'Vender Producto',
 });
});
/**
 * @author Rafael Arrieta Salcedo <arrietasalcedorafael@gmail.com>
 * @fecha_creacion 2021-06-12 
 * @desc metodo que permite buscar un producto para su posterior edición
 */
let buscarProducto = (productoId) => {
 $.ajax({
  type: "POST",
  url: "../controllers/productos.php",
  data: { opcion: 'ctrBuscarProducto', productoId },
  dataType: "json",
  success: function({ categoria, nombre_producto, peso, precio, producto_id, referencia, stock }) {
   $("#categoriaEditar").val(categoria)
   $("#nombreProductoEditar").val(nombre_producto)
   $("#pesoEditar").val(peso)
   $("#precioEditar").val(precio)
   $("#productoId").val(producto_id)
   $("#referenciaEditar").val(referencia)
   $("#stockEditar").val(stock)
  }
 });
}

/**
 * @author Rafael Arrieta Salcedo <arrietasalcedorafael@gmail.com>
 * @fecha_creacion 2021-06-12 
 * @desc metodo que permite elimanr un producto
 */
let EliminarProducto = (productoId) => {
 let opcion = confirm("Deseas eliminar el producto");
 if (opcion == true) {
  $.ajax({
   type: "POST",
   url: "../controllers/productos.php",
   data: { opcion: 'ctrEliminarProducto', productoId },
   dataType: "json",
   success: function(res) {
    if (res === "ok") {
     confirm("El producto ha sido eliminado correctamente");
     window.location = "index.php"
    } else {
     confirm("Error al elimanar el producto");
     window.location = "index.php"
    }
   }
  });

 }

}

let venderProducto = (productoId) => {
 let opcion = confirm("Deseas vender el producto");
 if (opcion == true) {
  $.ajax({
   type: "POST",
   url: "../controllers/productos.php",
   data: { opcion: 'ctrVenderProducto', productoId },
   dataType: "json",
   success: function({ error, mensaje }) {
    alert(mensaje)
    if (error) {
     return
    }
    window.location = "index.php"
   }
  });
 }

}