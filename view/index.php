<?php
include("../controllers/productos.php");
?>
<!DOCTYPE html>
<html lang="es-CO">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>::Inventario Productos::</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

</head>

<body>
    <div class="container">
        <h3 class="display-5 fw-normal">::. Autor: Rafael Arrieta Salcedo .::</h3>
        <h3 class="display-5 fw-normal">::. Inventario Productos .::</h3>
        <div class="row">
            <div class="col-12 col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCrearProducto">
                    Agregar Producto
                </button>
                <div class="table-responsive">
                    <table class=" table table-hover table-bordered mt-5" style="width: 100%;">
                        <thead class="table-success">
                            <tr class="text-center">
                                <th>#</th>
                                <th>PRODUCTO</th>
                                <th>REFERENCIA</th>
                                <th>PRECIO</th>
                                <th>PESO</th>
                                <th>CATEGORÍA</th>
                                <th>STOCK</th>
                                <th>FECHA CREACIÓN</th>
                                <th>FECHA ULTIMA VENTA</th>
                                <th>OPCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if (isset($productos)) {
                            foreach ($productos as $key => $item) {
                                echo "<tr class='text-center'>
                                    <td>" . ($key + 1) . "</td>
                                    <td>" . $item["nombre_producto"] . "</td>
                                    <td>" . $item["referencia"] . "</td>
                                    <td>" . $item["precio"] . "</td>
                                    <td>" . $item["peso"] . "</td>
                                    <td>" . $item["categoria"] . "</td>
                                    <td>" . $item["stock"] . "</td>
                                    <td>" . $item["fecha_creacion"] . "</td>
                                    <td>" . $item["fecha_ultima_venta"] . "</td>
                                    <td> <button onclick='buscarProducto(".$item["producto_id"].")' type='button' class='btn btn-warning' data-toggle='modal' data-target='#modalEditarProducto' id='btnEditarProducto'><i class='fa fa-pencil' aria-hidden='true'></i></button>
                                    <button onclick='EliminarProducto(".$item["producto_id"].")' type='button' class='btn btn-danger' id='btnEliminar'><i class='fa fa-trash' aria-hidden='true'></i></button>
                                    <button onclick='venderProducto(".$item["producto_id"].")' type='button' class='btn btn-success' id='btnVender'><i class='fa fa-money' aria-hidden='true'></i></button></td>
                                </tr>";
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                </ddiv>
            </div>
        </div>

        <!-- Modal para agregar un producto  -->
        <div class="modal fade" id="modalCrearProducto" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form role="form" method="post" action="?opcion=ctrCrearProducto">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Agregar Producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nombreProducto">Nombre Producto</label>
                                    <input type="text" class="form-control input-lg" name="nombreProducto"
                                        id="nombreProducto" placeholder="Ingresar el nombre del producto" required>
                                </div>
                                <div class="form-group">
                                    <label for="referencia">Referencia</label>
                                    <input type="text" class="form-control input-lg" name="referencia" id="referencia"
                                        placeholder="Ingresar Referencia" required>
                                </div>
                                <div class="form-group">
                                    <label for="precio">Precio</label>
                                    <input type="number" class="form-control input-lg" name="precio" id="precio"
                                        placeholder="Ingresar Precio" required>
                                </div>
                                <div class="form-group">
                                    <label for="peso">peso</label>
                                    <input type="number" class="form-control input-lg" name="peso" id="peso"
                                        placeholder="Ingresar Peso" required>
                                </div>
                                <div class="form-group">
                                    <label for="categoria">Categoria</label>
                                    <input type="text" class="form-control input-lg" name="categoria" id="categoria"
                                        placeholder="Ingresar Categoria" required>
                                </div>
                                <div class="form-group">
                                    <label for="stock">Stock</label>
                                    <input type="number" class="form-control input-lg" name="stock" id="stock"
                                        placeholder="Ingresar Stock" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                            <button type="submit" class="btn btn-primary">Guardar Producto</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Fin modal para agregar un producto  -->

        <!-- Modal para editar un producto  -->
        <div class="modal fade" id="modalEditarProducto" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form role="form" method="post" action="?opcion=ctrEditarProductos">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Editar Producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                                <input type="hidden" name="productoId" id="productoId" required>
                                <div class="form-group">
                                    <label for="nombreProductoEditar">Nombre Producto</label>
                                    <input type="text" class="form-control input-lg" name="nombreProductoEditar"
                                        id="nombreProductoEditar" placeholder="Ingresar el nombre del producto"
                                        required>
                                </div>
                                <div class="form-group">
                                    <label for="referenciaEditar">Referencia</label>
                                    <input type="text" class="form-control input-lg" name="referenciaEditar"
                                        id="referenciaEditar" placeholder="Ingresar Referencia" required>
                                </div>
                                <div class="form-group">
                                    <label for="precioEditar">Precio</label>
                                    <input type="number" class="form-control input-lg" name="precioEditar"
                                        id="precioEditar" placeholder="Ingresar Precio" required>
                                </div>
                                <div class="form-group">
                                    <label for="pesoEditar">peso</label>
                                    <input type="number" class="form-control input-lg" name="pesoEditar" id="pesoEditar"
                                        placeholder="Ingresar Peso" required>
                                </div>
                                <div class="form-group">
                                    <label for="categoriaEditar">Categoria</label>
                                    <input type="text" class="form-control input-lg" name="categoriaEditar"
                                        id="categoriaEditar" placeholder="Ingresar Categoria" required>
                                </div>
                                <div class="form-group">
                                    <label for="stockEditar">Stock</label>
                                    <input type="number" class="form-control input-lg" name="stockEditar"
                                        id="stockEditar" placeholder="Ingresar Stock" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                            <button type="submit" class="btn btn-primary">Editar Producto</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Fin modal para agregar un producto  -->

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
        </script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
        <script src="https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>
        <script src="js/productos.js"></script>

</body>

</html>