<?php
require_once('../models/ConfigDB.php');

class Producto
{
    /*
        * Autor: Rafael Arrieta Salcedo
        * Email: arrietasalcedorafael@gmail.com
        * Descripción: Metodo que permite listar todos los productos
        * Fecha Creación: 2021-06-12
    */
    public static function MdlListarProductos()
    {
        $query = "SELECT * FROM producto";
        $productos = Conexion::conectar()->prepare($query);
        $productos->execute();
        $data = $productos->fetchAll();
        $productos = null;
        return $data;
    }

    /*
        * Autor: Rafael Arrieta Salcedo
        * Email: arrietasalcedorafael@gmail.com
        * Descripción: Metodo que permite crear un producto
        * Fecha Creación: 2021-06-12
    */
    public static function MdlCrearProducto($data)
    {
        $query = "INSERT INTO producto(
                    nombre_producto,
                    referencia,
                    precio,
                    peso,
                    categoria,
                    stock
                )
                VALUES(
                    '{$data["nombreProducto"]}',
                    '{$data["referencia"]}',
                    '{$data["precio"]}',
                    '{$data["peso"]}',
                    '{$data["categoria"]}',
                    '{$data["stock"]}'
                )";
        $resultado = Conexion::conectar()->prepare($query);
        if ($resultado->execute()) {
            $info = "ok";
        } else {
            $info = "error";
        }
        $resultado = null;
        return $info;
    }

    /*
        * Autor: Rafael Arrieta Salcedo
        * Email: arrietasalcedorafael@gmail.com
        * Descripción: Metodo que permite buscar un producto por medio del parametro $productoId
        * Fecha Creación: 2021-06-12
    */
    public static function MdlBuscarProducto($productoId)
    {
        $query = "SELECT * FROM producto WHERE producto_id = $productoId";
        $producto = Conexion::conectar()->prepare($query);
        $producto->execute();
        $data = $producto->fetch();
        $producto = null;
        return $data;
    }

    /*
        * Autor: Rafael Arrieta Salcedo
        * Email: arrietasalcedorafael@gmail.com
        * Descripción: Metodo que permite editar un producto
        * Fecha Creación: 2021-06-12
    */
    public static function MdlEditarProducto($data)
    {
        $query = "UPDATE
                        producto
                    SET
                        nombre_producto = '{$data["nombreProductoEditar"]}',
                        referencia      = '{$data["referenciaEditar"]}',
                        precio          = '{$data["precioEditar"]}',
                        peso            = '{$data["pesoEditar"]}',
                        categoria       = '{$data["categoriaEditar"]}',
                        stock           = '{$data["stockEditar"]}'
                    WHERE
                        producto_id = '{$data["productoId"]}'";

        $resultado = Conexion::conectar()->prepare($query);
        if ($resultado->execute()) {
            $info = "ok";
        } else {
            $info = "error";
        }
        $resultado = null;
        return $info;
    }

    /*
        * Autor: Rafael Arrieta Salcedo
        * Email: arrietasalcedorafael@gmail.com
        * Descripción: Metodo que permite eliminar un producto por medio del parametro $productoId
        * Fecha Creación: 2021-06-12
    */
    public static function MdlEliminarProducto($productoId)
    {
        $query = "DELETE FROM producto WHERE producto_id = $productoId";
        $resultado = Conexion::conectar()->prepare($query);
        if ($resultado->execute()) {
            $info = "ok";
        } else {
            $info = "error";
        }
        $resultado = null;
        return $info;
    }

    /*
        * Autor: Rafael Arrieta Salcedo
        * Email: arrietasalcedorafael@gmail.com
        * Descripción: Metodo que permite vender un producto el cual es buscado por medio del parametro $productoId
        * Fecha Creación: 2021-06-12
    */
    public static function mdlVenderProducto($productoId)
    {
        $query = "SELECT stock FROM producto WHERE producto_id = $productoId";
        $resultado = Conexion::conectar()->prepare($query);
        $resultado->execute();
        $data = $resultado->fetch();
        if ($data["stock"] == 0) {
            $info["error"] = true;
            $info["mensaje"] = 'El producto no cuenta con stock';
        } else {
            $stock = $data["stock"] - 1;
            $vender = Conexion::conectar()->prepare("UPDATE producto SET stock = $stock,fecha_ultima_venta = NOW() WHERE producto_id = $productoId");
            $vender->execute();
             $info["error"] = false;
            $info["mensaje"] = 'Producto vendido';
        }
        return $info;
    }
}