<?php

class Conexion
{
    /*
        * Autor: Rafael Arrieta Salcedo
        * Email: arrietasalcedorafael@gmail.com
        * Descripción: Metodo que permite hacer la conexion a la base datos por medio de PDO 
        * Fecha Creación: 2021-06-12
    */
    public static function conectar()
    {
        $conexion = new PDO("mysql:host=localhost;dbname=inventario_productos", "root", "");
        $conexion->exec("set names utf8");
        return $conexion;
    }
}